package com.at.controller.system;


import com.alibaba.fastjson.JSONObject;
import com.at.common.annotation.AccessLimit;
import com.at.common.annotation.Pass;
import com.at.common.annotation.ValidationParam;
import com.at.config.ResponseHelper;
import com.at.config.ResponseModel;
import com.at.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class LoginController {

    @Autowired
    private IUserService userService;

    @PostMapping("/login")
    @Pass
    //5秒产生一个令牌,放入容量为0.3的令牌桶
    @AccessLimit(perSecond = 0.3, timeOut = 5000)
    public ResponseModel<Map<String, Object>> login(
            @ValidationParam("identity,password")@RequestBody JSONObject requestJson
            )  throws Exception {
        return ResponseHelper.succeed(userService.checkMobileAndPasswd(requestJson));
    }

    @GetMapping("/test")
    public ResponseModel<Map<String, String>> test() throws Exception{
        return ResponseHelper.succeed(new HashMap<>());
    }
}
