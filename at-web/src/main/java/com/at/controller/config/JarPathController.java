package com.at.controller.config;


import com.alibaba.fastjson.JSONObject;
import com.at.config.ResponseHelper;
import com.at.config.ResponseModel;
import com.at.repo.entity.config.JarPathEntity;
import com.at.repo.service.IJarPathService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class JarPathController {

    @Autowired
    private IJarPathService jarPathService;

    @GetMapping("/config/JarPath")
    public ResponseModel<List<JarPathEntity>> jarPathList(){
        QueryWrapper<JarPathEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("id");
        return ResponseHelper.succeed(jarPathService.list(queryWrapper));
    }

    @PostMapping("config/JarPath")
    public ResponseModel<Map<String, Object>> creataJarPath(
            @RequestBody JSONObject requestJson
            ) throws Exception {
        JarPathEntity jarPathEntity = (JarPathEntity) JSONObject.toJavaObject(requestJson, JarPathEntity.class);
        return ResponseHelper.createSucceed(jarPathService.createJarPath(jarPathEntity));
    }
}
