package com.at.controller.task;

import com.alibaba.fastjson.JSONObject;
import com.at.common.annotation.ValidationParam;
import com.at.config.ResponseHelper;
import com.at.config.ResponseModel;
import com.at.repo.entity.task.TaskBranches;
import com.at.repo.entity.task.TaskExecEntity;
import com.at.repo.entity.task.TaskInfoEntity;
import com.at.repo.entity.gitlab.TreeEntity;
import com.at.repo.service.*;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jacoco.core.analysis.IBundleCoverage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.spring.web.json.Json;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

@RestController
public class TaskController {

    private static final Logger logger = LogManager.getLogger(TaskController.class);

    @Autowired
    private ITaskInfoService taskInfoService;

    @Autowired
    private ITaskExecService taskExecService;

    @Autowired
    private ITaskExecBranchesService taskExecBranchesService;

    @Autowired
    private IJacgService jacgService;

    @Autowired
    private IGitlabService gitlabService;

    @Autowired
    private IScpAndUnzipService scpAndUnzipService;

    @PostMapping("/repo/task")
    public ResponseModel<Map<String, Object>> createTask(
            @ValidationParam("userNo, taskName, privateToken, webHost, projectId, jacocoIp, jacocoPort, jarPathConfig")
            @RequestBody JSONObject requestJson)
            throws Exception {
        TaskInfoEntity taskInfo = (TaskInfoEntity) JSONObject.toJavaObject(requestJson, TaskInfoEntity.class);
        return ResponseHelper.createSucceed(taskInfoService.createTask(taskInfo));
    }

    @GetMapping("/repo/task")
    public ResponseModel<List<TaskInfoEntity>> taskList() {
        return ResponseHelper.succeed(taskInfoService.list((Wrapper<TaskInfoEntity>) new QueryWrapper().orderByDesc("id")));
    }

    @GetMapping("/repo/task/{taskId}")
    public ResponseModel<TaskInfoEntity> taskInfoById(@PathVariable Integer taskId) {
        return ResponseHelper.succeed(taskInfoService.getById(taskId));
    }

    @GetMapping("/repo/task/{taskId}/exec")
    public ResponseModel<List<TaskExecEntity>> taskExecList(@PathVariable Integer taskId) {
        QueryWrapper<TaskExecEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("task_id", taskId);
        queryWrapper.orderByDesc("id");
        return ResponseHelper.succeed(taskExecService.list(queryWrapper));
    }

    @GetMapping("/repo/task/{taskId}/branches")
    public ResponseModel<List<TaskBranches>> taskExecBranches(@PathVariable Integer taskId) {
        return ResponseHelper.succeed(taskExecBranchesService.getBranches(taskId));
    }

    @PostMapping("/repo/task/{taskId}/exec")
    public ResponseModel<TaskExecEntity> createExec(
            @ValidationParam("fromBranch, toBranch, fromBranchCommit, toBranchCommit, jacocoIp, jacocoPort, classPath, jarLocation")
            @PathVariable Integer taskId,
            @RequestBody JSONObject requestJson) throws Exception{
        TaskExecEntity taskExec = (TaskExecEntity) JSONObject.toJavaObject(requestJson, TaskExecEntity.class);
        taskExec.setTaskId(taskId);
        TaskExecEntity ret = taskExecService.createExec(taskExec);
        scpAndUnzipService.scpAndUnzip(ret);
        return ResponseHelper.createSucceed(ret);
    }

    @GetMapping("/repo/task/{taskId}/exec/{execId}/tree")
    public ResponseModel<List<TreeEntity>> tree(@PathVariable Integer taskId, @PathVariable Integer execId) throws URISyntaxException {
        TaskInfoEntity taskInfo = taskInfoService.getById(taskId);
        TaskExecEntity taskExec = taskExecService.getById(execId);
        Boolean changeFlag = gitlabService.getGitlabDiff(taskInfo, taskExec);
        IBundleCoverage bundleCoverage = gitlabService.getBundleCoverage(
                taskExec.getJacocoIp(),
                taskExec.getJacocoPort(),
                taskExec.getClassPath());
        jacgService.runEnhanceRunnerGenAllGraph4Callee(taskInfo, taskExec, changeFlag);
        return ResponseHelper.succeed(gitlabService.getGitlabTree(taskInfo, taskExec, bundleCoverage));
    }

    @GetMapping("repo/task/{taskId}/exec/{execId}/fileDetail")
    public ResponseModel<Map> fileDetail(@PathVariable Integer taskId,
                                         @PathVariable Integer execId,
                                         @RequestParam(value = "file") String file) throws UnsupportedEncodingException, URISyntaxException {
        TaskInfoEntity taskInfo = taskInfoService.getById(taskId);
        TaskExecEntity taskExec = taskExecService.getById(execId);
        IBundleCoverage bundleCoverage = gitlabService.getBundleCoverage(
                taskExec.getJacocoIp(),
                taskExec.getJacocoPort(),
                taskExec.getClassPath());
        Map response = gitlabService.getGitlabFileDetail(taskInfo, taskExec, file);
        List<Map<String, Integer>> tmpLIst = gitlabService.getGitlabFileDiff(taskInfo, taskExec, file, bundleCoverage);
        List<Map<String, Integer>> chainList = gitlabService.getChainFileDiff(taskInfo, taskExec, file, bundleCoverage);
        response.put("diffList", tmpLIst);
        response.put("chainList", chainList);
        return ResponseHelper.succeed(response);
    }

    @GetMapping("repo/task/{taskId}/commits")
    public ResponseModel<JsonNode> commitsList(@PathVariable Integer taskId,
                                           @RequestParam(value = "refName") String refName) throws URISyntaxException {
        TaskInfoEntity taskInfo = taskInfoService.getById(taskId);
        JsonNode response = gitlabService.getCommitsByRef(taskInfo, refName);
        return ResponseHelper.succeed(response);
    }
}
