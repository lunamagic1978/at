package com.at.repo.service.impl;

import com.at.repo.entity.jacg.CalleeChainEntity;
import com.at.repo.mapper.CalleeChainMapper;
import com.at.repo.service.ICalleeChainService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.util.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 徐旻
 * @since 2022-07-19 16:51:30
 */
@Service
public class CalleeChainServiceImpl extends ServiceImpl<CalleeChainMapper, CalleeChainEntity> implements ICalleeChainService {

    @Override
    public int saveChain(int taskId, int execId, String level, String calleeFullName, String calleePackageName, String line, int parent, String commitId){
        CalleeChainEntity existCalleeChainEntity = baseMapper.selectOne(new QueryWrapper<CalleeChainEntity>()
                .eq("task_id", taskId)
                .eq("exec_id", execId)
                .eq("level", level)
                .eq("callee_full_name", calleeFullName)
                .eq("callee_package_name", calleePackageName)
                .eq("line", line)
                .eq("commit_id", commitId)
        );
        if (StringUtils.isEmpty(existCalleeChainEntity)){
            CalleeChainEntity calleeChainEntity = new CalleeChainEntity();
            calleeChainEntity.setTaskId(taskId);
            calleeChainEntity.setExecId(execId);
            calleeChainEntity.setLevel(level);
            calleeChainEntity.setCalleeFullName(calleeFullName);
            calleeChainEntity.setCalleePackageName(calleePackageName);
            calleeChainEntity.setLine(line);
            calleeChainEntity.setParent(parent);
            calleeChainEntity.setCommitId(commitId);
            baseMapper.insert(calleeChainEntity);
            return calleeChainEntity.getId();
        }else{
            return existCalleeChainEntity.getId();
        }
    }

    @Override
    public List<CalleeChainEntity> getChainListByFile(int execId, String commitId, String file){
        return baseMapper.selectList(new QueryWrapper<CalleeChainEntity>()
                .eq("exec_id", execId)
                .eq("commit_id", commitId)
                .eq("callee_package_name", file)
                .ne("level", "0")
        );
    }
}
