package com.at.repo.service.impl;

import com.at.repo.entity.task.TaskExecEntity;
import com.at.repo.mapper.TaskExecMapper;
import com.at.repo.service.ITaskExecService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 徐旻
 * @since 2022-05-25 10:21:05
 */
@Service
@EnableAsync
public class TaskExecServiceImpl extends ServiceImpl<TaskExecMapper, TaskExecEntity> implements ITaskExecService {

    @Autowired
    private TaskExecMapper taskExecMapper;

    @Override
    public TaskExecEntity createExec(TaskExecEntity taskExec) throws Exception{
        taskExec.setCreateTime(new Date());
        taskExec.setUpdateTime(new Date());
        taskExec.setStatus(0);
        taskExec.setIsDelete(0);
        taskExec.setJarStatus(0);
        this.save(taskExec);
        return taskExec;
    }

    @Override
    public void setCommitId(TaskExecEntity taskExec, String commitId){
        taskExec.setCommitId(commitId);
        baseMapper.updateById(taskExec);
    }

//    public void taskExecList() throws Exception{}
}
