package com.at.repo.service.impl;

import com.at.repo.entity.gitlab.GitlabDiffEntity;
import com.at.repo.mapper.GitlabDiffMapper;
import com.at.repo.service.IGitlabDiffService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 徐旻
 * @since 2022-07-22 17:38:35
 */
@Service
public class GitlabDiffServiceImpl extends ServiceImpl<GitlabDiffMapper, GitlabDiffEntity> implements IGitlabDiffService {

    @Override
    public void saveDiff(int execId, String commitId, String newPath, String path, int line, int contentLength){
        GitlabDiffEntity existGitlabDiffEntity = baseMapper.selectOne(
                new QueryWrapper<GitlabDiffEntity>()
                        .eq("exec_id", execId)
                        .eq("commit_id", commitId)
                        .eq("new_path", newPath)
                        .eq("path", path)
                        .eq("line", line)
        );
        if (StringUtils.isEmpty(existGitlabDiffEntity)){
            GitlabDiffEntity gitlabDiffEntity = new GitlabDiffEntity();
            gitlabDiffEntity.setExecId(execId);
            gitlabDiffEntity.setCommitId(commitId);
            gitlabDiffEntity.setNewPath(newPath);
            gitlabDiffEntity.setPath(path);
            gitlabDiffEntity.setLine(line);
            gitlabDiffEntity.setContentLength(contentLength);
            baseMapper.insert(gitlabDiffEntity);
        }
    }

    @Override
    public List<GitlabDiffEntity> getDiffListByFile (String file, int execId, String commitId){
        return baseMapper.selectList(new QueryWrapper<GitlabDiffEntity>()
                .eq("new_path", file)
                .eq("exec_id", execId)
                .eq("commit_id", commitId)
        );
    }

    @Override
    public List<GitlabDiffEntity> getDiffList (int execId, String commitId, String path){
        return baseMapper.selectList(new QueryWrapper<GitlabDiffEntity>()
                .eq("exec_id", execId)
                .eq("commit_id", commitId)
                .eq("path", path)
        );
    }

    @Override
    public List<GitlabDiffEntity> getDiffGroup(int execId, String commitId){
        return baseMapper.selectList(new QueryWrapper<GitlabDiffEntity>()
                .groupBy("path")
                .eq("exec_id", execId)
                .eq("commit_id", commitId));
    }
}
