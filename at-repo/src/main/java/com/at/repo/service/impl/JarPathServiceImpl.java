package com.at.repo.service.impl;

import com.at.repo.entity.config.JarPathEntity;
import com.at.repo.mapper.JarPathMapper;
import com.at.repo.service.IJarPathService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 徐旻
 * @since 2022-09-23 10:04:27
 */
@Service
public class JarPathServiceImpl extends ServiceImpl<JarPathMapper, JarPathEntity> implements IJarPathService {

    @Override
    public Map<String, Object>  createJarPath(JarPathEntity jarPathEntity) throws Exception{
        this.save(jarPathEntity);
        return new HashMap<>();
    }
}
