package com.at.repo.service;

import org.jacoco.core.analysis.Analyzer;
import org.jacoco.core.analysis.CoverageBuilder;
import org.jacoco.core.analysis.IBundleCoverage;
import org.jacoco.core.data.ExecutionDataStore;
import org.jacoco.core.tools.ExecDumpClient;
import org.jacoco.core.tools.ExecFileLoader;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
public class IJacocoService {

    public IBundleCoverage basicBundleCoverage(String address, Integer port, String classpath){
        List<String> classPathes = Arrays.asList(classpath.split(","));
        ExecDumpClient client = new ExecDumpClient();
        client.setDump(true);
        try {
            ExecFileLoader execFileLoader = client.dump(address, port);
            ExecutionDataStore executionDataStore = execFileLoader.getExecutionDataStore();
            final CoverageBuilder coverageBuilder = new CoverageBuilder();
            final Analyzer analyzer = new Analyzer(executionDataStore, coverageBuilder);
//            analyzer.analyzeAll(new File(classpath));
//            IBundleCoverage bundleCoverage = coverageBuilder.getBundle("demo");
            classPathes.forEach((e)->{
                try {
                    analyzer.analyzeAll(new File(e));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
            return coverageBuilder.getBundle("demo");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
