package com.at.repo.service;

import com.at.repo.entity.gitlab.GitlabDiffEntity;
import com.at.repo.entity.gitlab.GitlabTreeEntity;
import com.at.repo.entity.jacg.CalleeChainEntity;
import com.at.repo.entity.task.TaskExecEntity;
import com.at.repo.entity.task.TaskInfoEntity;
import com.at.repo.entity.gitlab.TreeEntity;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jacoco.core.analysis.IBundleCoverage;
import org.jacoco.core.analysis.IPackageCoverage;
import org.jacoco.core.analysis.ISourceFileCoverage;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.*;

@Service
public class IGitlabService {

    private static final Logger logger = LogManager.getLogger(IGitlabService.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private IJacocoService jacocoService;

    @Autowired
    private ITaskExecService taskExecService;

    @Autowired
    private IGitlabDiffService gitlabDiffService;

    @Autowired
    private ICalleeChainService calleeChainService;

    /**
     * 获取对应Gitlab仓库中的目录结构
     *
     * @return 目录结构
     */
    public List<TreeEntity> getGitlabTree(@NotNull TaskInfoEntity taskInfo, @NotNull TaskExecEntity taskExec, IBundleCoverage bundleCoverage){

        String nextPage;
        String webHost = taskInfo.getWebHost();
        Integer projectId = taskInfo.getProjectId();
        String PrivateToken = taskInfo.getPrivateToken();
        String uri = webHost + "/api/v4/projects/" + projectId + "/repository/tree?per_page=100&recursive=true&ref=" + taskExec.getToBranch();
        HttpHeaders headers = new HttpHeaders();
        headers.add("PRIVATE-TOKEN", PrivateToken);
        HttpEntity<Object> requestEntity = new HttpEntity<>(headers);

        List <TreeEntity> treeVoList = new ArrayList();
        List <GitlabTreeEntity> tmpList = new ArrayList<>();

        ResponseEntity<List<GitlabTreeEntity>> response = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<GitlabTreeEntity>>() {
        });
        tmpList.addAll(response.getBody());
        nextPage = response.getHeaders().get("X-Next-Page").get(0);

        while (!nextPage.isEmpty()){
            uri = uri + "&page=" + nextPage;
            response = restTemplate.exchange(uri, HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<GitlabTreeEntity>>() {
            });
            nextPage = response.getHeaders().get("X-Next-Page").get(0);
            tmpList.addAll(response.getBody());
        }

        for (GitlabTreeEntity x: tmpList){
            if (x.getType().equals("blob") && !x.getPath().startsWith(".idea"))
            {
                treeVoList = this.recursiveTree(x.getPath(), x.getPath(), treeVoList, taskExec, bundleCoverage);
            }
        }
        return treeVoList;
    }


    /**
     * 获取对应Gitlab仓库中的目录结构
     *
     * @return 目录结构
     */
    public List<TreeEntity> recursiveTree(String path, @NotNull String nextPath, List <TreeEntity> treeVoList, TaskExecEntity taskExec, IBundleCoverage bundleCoverage){
        String[] splitList = nextPath.split("/");
        if (splitList.length > 1) {
            int index = nextPath.indexOf("/");
            String next = nextPath.substring(index + 1);
            String now = nextPath.substring(0, index);
            TreeEntity tmpVo = new TreeEntity();
            tmpVo.setLabel(now);
            tmpVo.setName(now);
            tmpVo.setChildren(new ArrayList<TreeEntity>());
            boolean addFlag = true;
            for (TreeEntity item: treeVoList){
                if (item.getName().equals(now)){
                    tmpVo = item;
                    addFlag = false;
                }
            }

            List<TreeEntity> childNode = this.recursiveTree(path, next, tmpVo.getChildren(), taskExec, bundleCoverage);
            childNode.removeAll(tmpVo.getChildren());
            childNode.addAll(tmpVo.getChildren());
            tmpVo.setChildren(childNode);

            if (path.equals(nextPath)){
                if (addFlag){
                    treeVoList.add(tmpVo);
                }
                return treeVoList;
            }
            else{
                ArrayList<TreeEntity> tmpList = new ArrayList<TreeEntity>();
                tmpList.add(tmpVo);
                return tmpList;}
        }else{
            TreeEntity tmpVo = new TreeEntity();
            tmpVo.setLabel(nextPath);
            tmpVo.setName(nextPath);
            tmpVo.setPath(path);
            if (path.equals(nextPath)){
                treeVoList.add(tmpVo);
                return treeVoList;
            }
            else{
                ArrayList<TreeEntity> tmpList = new ArrayList<TreeEntity>();
                List<GitlabDiffEntity> diffList = gitlabDiffService.getDiffListByFile(path, taskExec.getId(), taskExec.getCommitId());
                int tmpDiff = 0;
                int tmpDiffPass = 0;
                for (GitlabDiffEntity diff: diffList){
                    int status = this.getInstructionStatus(diff.getNewPath(), bundleCoverage, diff.getLine());
                    if (status == 2){
                        tmpDiffPass = tmpDiffPass + 1;
                    }
                    else if (status != 0){
                        tmpDiff = tmpDiff + 1;
                    }
                }
                int tmpChain = 0;
                int tmpChainPass = 0;
                List<CalleeChainEntity> chainList = calleeChainService.getChainListByFile(taskExec.getId(), taskExec.getCommitId(), this.pathFormatter(path).replace("/", "."));
                for (CalleeChainEntity chain: chainList){
                    int status = this.getInstructionStatus(path, bundleCoverage, Integer.parseInt(chain.getLine()));
                    if (status == 2){
                        tmpChainPass = tmpChainPass + 1;
                    }
                    else if (status != 0){
                        tmpChain = tmpChain + 1;
                    }
                }
                tmpVo.setShow(Boolean.TRUE);
                tmpVo.setDiff(tmpDiff);
                tmpVo.setDiffPass(tmpDiffPass);
                tmpVo.setChain(tmpChain);
                tmpVo.setChainPass(tmpChainPass);
                tmpList.add(tmpVo);
                return tmpList;
            }

        }
    }

    public HashMap getGitlabFileDetail(@NotNull TaskInfoEntity taskInfo, @NotNull TaskExecEntity taskExec, String file) throws URISyntaxException, UnsupportedEncodingException {

        String webHost = taskInfo.getWebHost();
        Integer projectId = taskInfo.getProjectId();
        String PrivateToken = taskInfo.getPrivateToken();
        String fileEncode = URLEncoder.encode(file, "GBK");
        String urlEncode =  webHost + "/api/v4/projects/" + projectId + "/repository/files/" + fileEncode + "?ref=" + taskExec.getToBranch();

        HttpHeaders headers = new HttpHeaders();
        headers.add("PRIVATE-TOKEN", PrivateToken);
        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        ResponseEntity<HashMap> exchange = restTemplate.exchange(new URI(urlEncode), HttpMethod.GET, httpEntity,
                HashMap.class);
        String content = exchange.getBody().get("content").toString();

        final Base64.Decoder decoder = Base64.getDecoder();
        String decodeContent = new String(decoder.decode(content), "UTF-8");
        exchange.getBody().put("content", decodeContent);
        return exchange.getBody();
    }

    public IBundleCoverage getBundleCoverage(String address, Integer port, String classpath){
        return jacocoService.basicBundleCoverage(address, port, classpath);
    }

    /**
     * 获取对应Gitlab仓库中的对应版本的diff，并且保存。
     *
     * @return
     */
    public Boolean getGitlabDiff(TaskInfoEntity taskInfo, TaskExecEntity taskExec) throws URISyntaxException {
        boolean changeFlag = false;

        // 请求gitlab接口，获取两个版本之间的diff
        String urlEncode = taskInfo.getWebHost() + "/api/v4/projects/" + taskInfo.getProjectId() + "/repository/compare?from=" + taskExec.getFromBranchCommit() + "&to=" + taskExec.getToBranchCommit();
        HttpHeaders headers = new HttpHeaders();
        headers.add("PRIVATE-TOKEN", taskInfo.getPrivateToken());
        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        logger.info("request start, url:" + urlEncode);
        ResponseEntity<JsonNode> exchange = restTemplate.exchange(new URI(urlEncode), HttpMethod.GET, httpEntity,
                JsonNode.class);
        JsonNode respContent = Objects.requireNonNull(exchange.getBody());
        System.out.println(respContent.toString());
        System.out.println(respContent.get("commit").isNull());
        // 如果commitId为空 或者commitId不一致，解析增加的代码，保存相关信息。
        if (respContent.get("commit").isNull()){
            System.out.println("no commit");
            return changeFlag;
        }
        if (taskExec.getCommitId() == null || !Objects.equals(taskExec.getCommitId(), respContent.get("commit").get("id").asText())){
            // 初始化起始位置和状态
            int startPos = 0;
            changeFlag = true;

            // 把新的commitId保存到对应的执行信息中
            taskExecService.setCommitId(taskExec, respContent.get("commit").get("id").asText());

            // 解析diff
            for(JsonNode diff: respContent.get("diffs")){
                // 获取newPath并且formatter成path
                String newPath = diff.get("new_path").asText();
                String path = this.pathFormatter(newPath);
                // 获取diff，并且通过@@对diff的内容进行分割
                String diffContent = diff.get("diff").asText();
                String[] splitList = diffContent.split("@@");
                // 解析分割内容
                for (String item: splitList){
                    // 如果开头是空格加上-， 这里的内容是对应修改的范围，通过解析或者+后的位置作为起始位置
                    if (item.startsWith(" -")){
                        String[] tmp = item.split(",");
                        String[] pos = tmp[1].split("\\+");
                        startPos = Integer.parseInt(pos[1]) - 1;
                    }
                    // 其他情况都是内容，通过\\n 来进行分行操作，然后解析
                    else{
                        String[] contentList = item.split("\\n");
                        int line = startPos;
                        for (String content: contentList){
                            // 如果行的内容不为空，开始进行解析操作
                            if (!content.isEmpty()){
                                // 行内容是+开头的情况，是新增的代码， 把相关信息保存到数据库
                                if (content.startsWith("+")){
                                    gitlabDiffService.saveDiff(
                                            taskExec.getId(),
                                            respContent.get("commit").get("id").asText(),
                                            newPath,
                                            path,
                                            line,
                                            content.length()
                                    );
                                    line = line + 1;
                                }
                                // 行内容是-开头的情况，是被删除的代码，不需要考虑 todo 被删除的代码是否也需要检查一下相关呢链路
                                else if(content.startsWith("-")){
                                    System.out.println("start with -");
                                }
                                // 其他行内容属于没有变化的情况，不做处理，但是行号+1
                                else {
                                    line = line + 1;
                                }
                            }
                            // 内容为空的行也要进行行号+1 操作
                            else{
                                line = line + 1;
                            }
                        }
                    }
                }
        }}
        return changeFlag;
    }

    /**
     * 获取指定文件的diff内容以及通过jacoco代理获取到对应行的执行状态
     *
     * @return 以行为单位的diff内容列表， line代表行，ch代表行的长度，status代表该行的执行状态
     * status状态
     * 0：不需要执行
     * 1：没有执行
     * 2：执行过
     * 3：执行一半（在有分支的情况下）
     */
    public List<Map<String, Integer>> getGitlabFileDiff(@NotNull TaskInfoEntity taskInfo, @NotNull TaskExecEntity taskExec, String file, IBundleCoverage bundleCoverage) throws URISyntaxException {
        // 初始化状态和返回列表
        int status = 0;
        List<Map<String, Integer>> retList = new ArrayList();

        // 拿到指定文件的diff内容
        List<GitlabDiffEntity> diffList = gitlabDiffService.getDiffListByFile(file, taskExec.getId(), taskExec.getCommitId());
        for (GitlabDiffEntity diff: diffList){
            // 如果jacoco的覆盖率为空，设置状态为1
            if (bundleCoverage == null){
                status = 1;
            }
            // 获取行号对应的状态
            else{
                status = this.getInstructionStatus(file, bundleCoverage, diff.getLine());
            }
            // 把需要的信息组装到临时map，添加的返回列表中
            Map<String, Integer> tmp = new HashMap<String, Integer>();
            tmp.put("line", diff.getLine());
            tmp.put("ch", diff.getContentLength());
            tmp.put("status", status);
            retList.add(tmp);
        }
        return retList;
    }

    public List<Map<String, Integer>> getChainFileDiff(TaskInfoEntity taskInfo, TaskExecEntity taskExec, String file, IBundleCoverage bundleCoverage) {
        List<Map<String, Integer>> retList = new ArrayList();
        int status = 0;

        List<CalleeChainEntity> calleeChainList = calleeChainService.getChainListByFile(taskExec.getId(), taskExec.getCommitId(), this.pathFormatter(file).replace("/", "."));
        for (CalleeChainEntity callChain: calleeChainList){
            if (bundleCoverage == null){
                status = 1;
            }
            else{
                status =this.getInstructionStatus(file, bundleCoverage, Integer.parseInt(callChain.getLine()));
            }
            Map<String, Integer> tmp = new HashMap<String, Integer>();
            tmp.put("line", Integer.parseInt(callChain.getLine()));
            tmp.put("ch", 20);
            tmp.put("status", status);
            retList.add(tmp);
        }
        return retList;
    }

    public Integer getInstructionStatus(String file, IBundleCoverage bundleCoverage, Integer line){
        int ret = 0;
        if (file.startsWith("src/main/java/")){
            file = file.substring("src/main/java/".length());
        }
        int index = file.lastIndexOf("/");
        String packageName = file.substring(0, index);
        String fileName = file.substring(index + 1);
        for (IPackageCoverage packageCoverage: bundleCoverage.getPackages()){
            if (packageCoverage.getName().equals(packageName)){
                for (ISourceFileCoverage sourceFileCoverage: packageCoverage.getSourceFiles()){
                    if (sourceFileCoverage.getName().equals(fileName)){
                        ret = sourceFileCoverage.getLine(line).getStatus();
                    }
                }
            }
        }
        return ret;
    }

    private String pathFormatter(String file){
        if (file.startsWith("src/main/java/")){
            return file.substring("src/main/java/".length()).split("\\.")[0];
        }
        return file;
    }

    public JsonNode getCommitsByRef(TaskInfoEntity taskInfo, String refName) throws URISyntaxException {
        String webHost = taskInfo.getWebHost();
        Integer projectId = taskInfo.getProjectId();
        String PrivateToken = taskInfo.getPrivateToken();
        String urlEncode =  webHost + "/api/v4/projects/" + projectId + "/repository/commits/?ref_name=" + refName;
        HttpHeaders headers = new HttpHeaders();
        headers.add("PRIVATE-TOKEN", PrivateToken);
        HttpEntity<Object> httpEntity = new HttpEntity<>(headers);
        ResponseEntity<JsonNode> exchange = restTemplate.exchange(new URI(urlEncode), HttpMethod.GET, httpEntity,
                JsonNode.class);
        return exchange.getBody();
    }
}
