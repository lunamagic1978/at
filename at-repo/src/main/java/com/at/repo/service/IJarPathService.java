package com.at.repo.service;

import com.at.repo.entity.config.JarPathEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 徐旻
 * @since 2022-09-23 10:04:27
 */
public interface IJarPathService extends IService<JarPathEntity> {
    Map<String, Object> createJarPath(JarPathEntity requestJson) throws Exception;
}
