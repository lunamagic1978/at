package com.at.repo.service;


import com.adrninistrator.jacg.common.JACGConstants;
import com.adrninistrator.jacg.common.enums.OutputDetailEnum;
import com.adrninistrator.jacg.dto.entity.ConfigureWrapperEntity;
import com.adrninistrator.jacg.dto.entity.OtherConfigFileUseListEntity;
import com.adrninistrator.jacg.dto.entity.OtherConfigFileUseSetEntity;
import com.adrninistrator.jacg.enhance.EnhanceCalleeChain;
import com.adrninistrator.jacg.enhance.EnhanceRunnerGenAllGraph4Callee;
import com.adrninistrator.jacg.enhance.EnhanceWriteDb;
import com.adrninistrator.jacg.unzip.UnzipFile;
import com.at.repo.entity.gitlab.GitlabDiffEntity;
import com.at.repo.entity.task.TaskExecEntity;
import com.at.repo.entity.task.TaskInfoEntity;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class IJacgService {

    @Autowired
    private ICalleeChainService calleeChainService;

    @Autowired
    private IGitlabDiffService gitlabDiffService;

    public void runEnhanceRunnerGenAllGraph4Callee(TaskInfoEntity taskInfo, TaskExecEntity taskExec, Boolean changeFlag){

//        String[] args = new String[0];
        final int[] parentId = {0};
//        UnzipFile.main(args);

        if (changeFlag){
            ConfigureWrapperEntity configureWrapperEntity = new ConfigureWrapperEntity();
            configureWrapperEntity.setAppName("Task" + taskInfo.getId() + "_" + "Exec" + taskExec.getId());
            configureWrapperEntity.setCallGraphJarList(taskExec.getJarLocation());
            configureWrapperEntity.setInputIgnoreOtherPackage(Boolean.FALSE.toString());
            configureWrapperEntity.setCallGraphOutputDetail(OutputDetailEnum.ODE_2.getDetail());
            configureWrapperEntity.setThreadNum("20");
            configureWrapperEntity.setShowMethodAnnotation(Boolean.TRUE.toString());
            configureWrapperEntity.setGenCombinedOutput(Boolean.TRUE.toString());
            configureWrapperEntity.setShowCallerLineNum(Boolean.TRUE.toString());
            configureWrapperEntity.setIgnoreDupCallerInOneCaller(Boolean.FALSE.toString());
            configureWrapperEntity.setDbUserH2(Boolean.FALSE.toString());
            configureWrapperEntity.setDbH2FilePath("./jacg_h2db_rbc");
            configureWrapperEntity.setDbDriverName("com.mysql.cj.jdbc.Driver");
            configureWrapperEntity.setDbUrl("jdbc:mysql://127.0.0.1:3306/test_db?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai&rewriteBatchedStatements=true");
            configureWrapperEntity.setDbUsername("root");
            configureWrapperEntity.setDbPassword("123456");
//            configureWrapperEntity.setDbUrl("jdbc:mysql://124.222.163.214:3306/callgraph?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai&rewriteBatchedStatements=true");
//            configureWrapperEntity.setDbPassword("52021314Jo");

            OtherConfigFileUseSetEntity otherConfigFileUseSetEntity = new OtherConfigFileUseSetEntity();
            otherConfigFileUseSetEntity.setInAllowedClassPrefix(new HashSet<>(Collections.emptyList()));
            otherConfigFileUseSetEntity.setOutGraphForCallerEntryMethod(new HashSet<>(Collections.emptyList()));
            otherConfigFileUseSetEntity.setOutGraphForCallerEntryMethodIgnorePrefix(new HashSet<>(Collections.emptyList()));
            otherConfigFileUseSetEntity.setOutGraphForCallerIgnoreClassKeyword(new HashSet<>(Collections.emptyList()));
            otherConfigFileUseSetEntity.setOutGraphForCallerIgnoreFullMethodPrefix(new HashSet<>(Collections.emptyList()));
            otherConfigFileUseSetEntity.setOutGraphForCallerIgnoreMethodPrefix(new HashSet<>(Collections.emptyList()));
            otherConfigFileUseSetEntity.setExtensionsCodeParser(new HashSet<>(Collections.emptyList()));
            otherConfigFileUseSetEntity.setExtensionsMethodAnnotationHandler(new HashSet<>(Collections.emptyList()));


            OtherConfigFileUseListEntity otherConfigFileUseListEntity = new OtherConfigFileUseListEntity();
            otherConfigFileUseListEntity.setFindKeyword4Callee(Collections.emptyList());
            otherConfigFileUseListEntity.setFindKeyword4Caller(Collections.emptyList());

            EnhanceWriteDb.enhanceWriteDb(configureWrapperEntity);

            EnhanceRunnerGenAllGraph4Callee enhanceRunnerGenAllGraph4Callee = new EnhanceRunnerGenAllGraph4Callee();

            List<GitlabDiffEntity> diffGroupList = gitlabDiffService.getDiffGroup(taskExec.getId(), taskExec.getCommitId());
            for (GitlabDiffEntity diffGroup: diffGroupList){
                if (diffGroup.getNewPath().startsWith("src")){
                    List<GitlabDiffEntity> gitlabDiffEntityList = gitlabDiffService.getDiffList(taskExec.getId(), taskExec.getCommitId(), diffGroup.getPath());
                    List<String> calleeClassNameList = new ArrayList<>();
                    for (GitlabDiffEntity gitlabDiffEntity: gitlabDiffEntityList){
                        calleeClassNameList.add(gitlabDiffEntity.getPath().replace("/", ".") + JACGConstants.FLAG_COLON + gitlabDiffEntity.getLine());
                    }
                    otherConfigFileUseSetEntity.setOutGraphForCalleeClassName(new HashSet<>(calleeClassNameList));
                    Map<String, List<Pair<String, Boolean>>>  callerMap = enhanceRunnerGenAllGraph4Callee.runByCode(configureWrapperEntity, otherConfigFileUseSetEntity, otherConfigFileUseListEntity);
                    if (callerMap != null){
                        for (String key: callerMap.keySet()){
                            if (callerMap.get(key) != null){
                                ArrayList<List<String>> callChain = new EnhanceCalleeChain().enhanceCalleeChain(callerMap.get(key));
                                callChain.forEach((e)->{
                                    e.forEach((b)->{
                                        String level = this.getLevel(b);
                                        String line = this.getLine(b);
                                        String fullName = this.getFullName(b);
                                        String packageName = this.getPackageName(fullName);
                                        if (Integer.parseInt(level) == 0){
                                            parentId[0] = 0;
                                        }
                                        parentId[0] = calleeChainService.saveChain(taskInfo.getId(), taskExec.getId(), level, fullName, packageName, line, parentId[0], taskExec.getCommitId());
                                    });
                                });
                            }}
                    }
                }


            }}
    }

    /**
     * 根据传入的内容返回对应的层级
     *
     * [0]#com.example.demo.service.ThirdService:thirdDemo
     * [1]#  com.example.demo.controller.ThirdController:third@org.springframework.web.bind.annotation.GetMapping	(ThirdController:27)
     *
     * 第一行返回值为0
     * 第二行返回值为1
     */
    private String getLevel(String content){
        Pattern pattern = Pattern.compile("(?<=\\[)[^}]*(?=\\])");
        Matcher matcher = pattern.matcher(content);
        if(matcher.find()){
            return matcher.group();
        }
        return "";
    }

    /**
     * 根据传入的内容返回对应的行号
     *
     * [0]#com.example.demo.service.ThirdService:thirdDemo
     * [1]#  com.example.demo.controller.ThirdController:third@org.springframework.web.bind.annotation.GetMapping	(ThirdController:27)
     *
     * 第一行返回值为空字符
     * 第二行返回值为27
     */
    private String getLine(String content){
        Pattern pattern = Pattern.compile("(?<=\\()[^}]*(?=\\))");
        Matcher matcher = pattern.matcher(content);
        if(matcher.find()){
            return matcher.group().substring(matcher.group().lastIndexOf(":") + 1);
        }
        return "";
    }

    /**
     * 根据传入的内容返回全名
     *
     * [0]#com.example.demo.service.ThirdService:thirdDemo
     * [1]#  com.example.demo.controller.ThirdController:third@org.springframework.web.bind.annotation.GetMapping	(ThirdController:27)
     *
     * 第一行返回为 com.example.demo.service.ThirdService:thirdDemo
     * 第二行返回为 com.example.demo.controller.ThirdController:third@org.springframework.web.bind.annotation.GetMapping	(ThirdController:27)
     */
    private String getFullName(String content){
        if (content.contains("[0]#")){
            String[] tmpString = content.split("#");
            return tmpString[1];
        } else {
            String[] tmpString = content.split(" ");
            return tmpString[2];
        }
    }

    /**
     * 根据传入的全名返回包名
     *
     * [0]#com.example.demo.service.ThirdService:thirdDemo
     * [1]#  com.example.demo.controller.ThirdController:third@org.springframework.web.bind.annotation.GetMapping	(ThirdController:27)
     *
     * 第一行返回为 com.example.demo.service.ThirdService
     * 第二行返回为 com.example.demo.controller.ThirdController
     */
    private String getPackageName(String content){
        String[] tmpString = content.split(":");
        return tmpString[0];
    }
}
