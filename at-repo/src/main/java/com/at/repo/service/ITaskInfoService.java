package com.at.repo.service;

import com.at.repo.entity.task.TaskInfoEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

public interface ITaskInfoService extends IService<TaskInfoEntity> {

    Map<String, Object> createTask(TaskInfoEntity requestJson) throws Exception;
}
