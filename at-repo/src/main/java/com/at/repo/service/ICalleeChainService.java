package com.at.repo.service;

import com.at.repo.entity.jacg.CalleeChainEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 徐旻
 * @since 2022-07-19 16:51:30
 */
public interface ICalleeChainService extends IService<CalleeChainEntity> {


    int saveChain(int taskId, int execId, String level, String calleeFullName, String calleePackageName, String line, int parent, String commitId);

    List<CalleeChainEntity> getChainListByFile(int execId, String commitId, String file);

}
