package com.at.repo.service;

import com.at.repo.entity.task.TaskBranches;
import com.at.repo.entity.task.TaskInfoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class ITaskExecBranchesService {

    @Autowired
    private ITaskInfoService taskInfoService;

    @Autowired
    private RestTemplate restTemplate;

    public List<TaskBranches> getBranches (Integer taskId) {
        TaskInfoEntity taskInfo = taskInfoService.getById(taskId);
        return this.getBranchesByTaskInfo(taskInfo);
    }

    private List<TaskBranches> getBranchesByTaskInfo (TaskInfoEntity taskInfo) {
        String uri = taskInfo.getWebHost() + "/api/v4/projects/" + taskInfo.getProjectId() + "/repository/branches";
        HttpHeaders headers = new HttpHeaders();
        headers.add("PRIVATE-TOKEN", taskInfo.getPrivateToken());
        HttpEntity<Object> requestEntity = new HttpEntity<>(headers);
        ResponseEntity<List<TaskBranches>> response = restTemplate.exchange(uri, HttpMethod.GET, requestEntity,
                new ParameterizedTypeReference<List<TaskBranches>>(){});
        return response.getBody();
    }
}
