package com.at.repo.service;

import com.at.repo.entity.config.JarPathEntity;
import com.at.repo.entity.task.TaskExecEntity;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.at.util.FileUtil;
import ch.ethz.ssh2.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

@Service
public class IScpAndUnzipService {

    private static final Logger logger = LogManager.getLogger(IScpAndUnzipService.class);

    @Autowired
    private IJarPathService jarPathService;

    @Autowired
    private ITaskExecService taskExecService;

    @Async
    public void scpAndUnzip(TaskExecEntity taskExecEntity) throws InterruptedException {
        JarPathEntity jarPathEntity = jarPathService.getById(taskExecEntity.getJarPathConfig());
        if (jarPathEntity.getType() == 3){
            String jarFileFold = System.getProperty("user.dir") + "/jarPath/task" + taskExecEntity.getTaskId() + "/exec" + taskExecEntity.getId();
            if (FileUtil.isDirectoryExists(jarFileFold)){
                String saveJarPath = FileUtil.formatScpFileSavePath(jarPathEntity.getPath(), jarFileFold);
                if (scpFile(jarPathEntity, saveJarPath, taskExecEntity)){
                    TaskExecEntity tmp = taskExecService.getById(taskExecEntity.getId());
                    logger.info(String.format("下载%s文件成功", tmp.getJarLocation()));
                    if (unzipJar(jarFileFold + "/classes", tmp)){
                        logger.info("文件解压成功");
                    }
                }
            }
        }
    }

    private boolean scpFile(JarPathEntity jarPathEntity, String local, TaskExecEntity taskExecEntity){
        Connection con = new Connection(jarPathEntity.getRemoteIp());
        try {
            con.connect();
            boolean isAuthed = con.authenticateWithPublicKey(jarPathEntity.getUsername(), new File(jarPathEntity.getPublicKey()), null);
            if (!isAuthed){
                return false;
            }
            SCPClient scpClient = con.createSCPClient();
            SCPInputStream res = scpClient.get(jarPathEntity.getPath());

            FileOutputStream fos = new FileOutputStream(local);
            byte[] buffer = new byte[1024];
            int len = 0;
            while((len = res.read(buffer)) != -1){
                fos.write(buffer, 0, len);
            }
            fos.close();
            con.close();
            UpdateWrapper<TaskExecEntity> updateWrapper = new UpdateWrapper<TaskExecEntity>()
                    .set("jar_status", 1)
                    .set("jar_location", local)
                    .eq("id", taskExecEntity.getId());
            taskExecService.update(taskExecEntity, updateWrapper);
            return true;
        } catch (Exception e){
            e.printStackTrace();
            UpdateWrapper<TaskExecEntity> updateWrapper = new UpdateWrapper<TaskExecEntity>()
                    .set("jar_status", 3)
                    .eq("id", taskExecEntity.getId());
            taskExecService.update(taskExecEntity, updateWrapper);
        }
        return false;
    }

    private boolean unzipJar(String local, TaskExecEntity taskExecEntity){
        try {
            File file = new File(taskExecEntity.getJarLocation());
            JarFile jar = new JarFile(file);

            // fist get all directories,
            // then make those directory on the destination Path
            for (Enumeration<JarEntry> enums = jar.entries(); enums.hasMoreElements();) {
                JarEntry entry = (JarEntry) enums.nextElement();

                String fileName = local + File.separator + entry.getName();
                File f = new File(fileName);

                if (fileName.endsWith("/")) {
                    f.mkdirs();
                }

            }

            //now create all files
            for (Enumeration<JarEntry> enums = jar.entries(); enums.hasMoreElements();) {
                JarEntry entry = (JarEntry) enums.nextElement();

                String fileName = local + File.separator + entry.getName();
                File f = new File(fileName);

                if (!fileName.endsWith("/")) {
                    InputStream is = jar.getInputStream(entry);
                    FileOutputStream fos = new FileOutputStream(f);

                    // write contents of 'is' to 'fos'
                    while (is.available() > 0) {
                        fos.write(is.read());
                    }

                    fos.close();
                    is.close();
                }
            }
            UpdateWrapper<TaskExecEntity> updateWrapper = new UpdateWrapper<TaskExecEntity>()
                    .set("jar_status", 2)
                    .set("class_path", local  + "/BOOT-INF/classes")
                    .eq("id", taskExecEntity.getId());
            taskExecService.update(taskExecEntity, updateWrapper);
            return true;

        } catch (Exception e){
            e.printStackTrace();
            UpdateWrapper<TaskExecEntity> updateWrapper = new UpdateWrapper<TaskExecEntity>()
                    .set("jar_status", 4)
                    .eq("id", taskExecEntity.getId());
            taskExecService.update(taskExecEntity, updateWrapper);
            return false;
        }
    }
}
