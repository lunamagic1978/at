package com.at.repo.service;

import com.at.repo.entity.gitlab.GitlabDiffEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 徐旻
 * @since 2022-07-22 17:38:35
 */
public interface IGitlabDiffService extends IService<GitlabDiffEntity> {

    void saveDiff(int execId, String commitId, String newPath, String path, int line, int contentLength);

    List<GitlabDiffEntity> getDiffListByFile(String file, int execId, String commitId);

    List<GitlabDiffEntity> getDiffList(int execId, String commitId, String path);

    List<GitlabDiffEntity> getDiffGroup(int execId, String commitId);
}
