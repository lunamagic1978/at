package com.at.repo.service.impl;

import com.at.repo.entity.task.TaskInfoEntity;
import com.at.repo.mapper.TaskInfoMapper;
import com.at.repo.service.ITaskInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class TaskInfoServiceImpl extends ServiceImpl<TaskInfoMapper, TaskInfoEntity> implements ITaskInfoService {

    @Autowired
    private TaskInfoMapper taskInfoMapper;

    @Override
    public Map<String, Object> createTask(TaskInfoEntity taskInfo)throws Exception{
        taskInfo.setCreateTime(new Date());
        taskInfo.setUpdateTime(new Date());
        this.save(taskInfo);
        return new HashMap<>();
    }

    public void taskList() throws Exception{}
}
