package com.at.repo.service;

import com.at.repo.entity.task.TaskExecEntity;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 徐旻
 * @since 2022-05-25 10:21:05
 */
public interface ITaskExecService extends IService<TaskExecEntity> {

    TaskExecEntity createExec(TaskExecEntity requestJson) throws Exception;

    void setCommitId(TaskExecEntity taskExec, String commitId);
}
