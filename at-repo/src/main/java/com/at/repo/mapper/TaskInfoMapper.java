package com.at.repo.mapper;

import com.at.repo.entity.task.TaskInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface TaskInfoMapper extends BaseMapper<TaskInfoEntity> {

}
