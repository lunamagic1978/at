package com.at.repo.mapper;

import com.at.repo.entity.jacg.CalleeChainEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 徐旻
 * @since 2022-07-19 16:51:30
 */
public interface CalleeChainMapper extends BaseMapper<CalleeChainEntity> {

}
