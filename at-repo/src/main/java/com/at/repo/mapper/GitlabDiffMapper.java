package com.at.repo.mapper;

import com.at.repo.entity.gitlab.GitlabDiffEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 徐旻
 * @since 2022-07-22 17:38:35
 */
public interface GitlabDiffMapper extends BaseMapper<GitlabDiffEntity> {

}
