package com.at.repo.mapper;

import com.at.repo.entity.task.TaskExecEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 徐旻
 * @since 2022-05-25 10:21:05
 */
public interface TaskExecMapper extends BaseMapper<TaskExecEntity> {

}
