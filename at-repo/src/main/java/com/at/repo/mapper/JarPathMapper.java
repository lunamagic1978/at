package com.at.repo.mapper;

import com.at.repo.entity.config.JarPathEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 徐旻
 * @since 2022-09-23 10:04:27
 */
public interface JarPathMapper extends BaseMapper<JarPathEntity> {

}
