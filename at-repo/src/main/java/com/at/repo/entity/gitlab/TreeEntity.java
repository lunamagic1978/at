package com.at.repo.entity.gitlab;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class TreeEntity implements Serializable {

    private String name;
    private String label;
    private String path;
    private Boolean show;
    private int diffPass;
    private int chainPass;
    private int diff;
    private int chain;
    private List<TreeEntity> children;

}
