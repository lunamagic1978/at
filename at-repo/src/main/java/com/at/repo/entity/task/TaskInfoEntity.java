package com.at.repo.entity.task;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@TableName("tb_task_info")
public class TaskInfoEntity extends Model<TaskInfoEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户Id
     */
    private String userNo;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 任务描述
     */
    private String taskDesc;

    /**
     * 代码仓库对应的私钥
     */
    private String privateToken;

    /**
     * 代码仓库地址
     */
    private String webHost;

    /**
     * 代码仓库项目id
     */
    private Integer projectId;

    /**
     * jacocoAgent的IP地址
     */
    private String jacocoIp;

    /**
     * jacocoAgent的Port
     */
    private Integer jacocoPort;

    /**
     * jacoco分析需要的class路径
     */
    private String classPath;

    /**
     * jacoco分析需要jar包的路径
     */
    private String jarLocation;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
