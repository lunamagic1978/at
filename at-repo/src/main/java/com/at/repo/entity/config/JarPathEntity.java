package com.at.repo.entity.config;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.*;

/**
 * <p>
 * 
 * </p>
 *
 * @author 徐旻
 * @since 2022-09-23 10:04:27
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@TableName("tb_jar_path")
public class JarPathEntity extends Model<JarPathEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * jarPath对应的名称
     */
    private String name;

    /**
     * jarPath对应的描述
     */
    private String description;

    /**
     * jarPath对应的type类型
     * 1：本地
     * 2：远程（用户名和密码）
     * 3：远程（用户名和key）
     */
    private Integer type;

    /**
     * jarPath对应的路径
     */
    private String path;

    /**
     * 放置jar包的远程服务器Ip
     */
    private String remoteIp;

    /**
     * 登录远程服务器的用户名
     */
    private String username;

    /**
     * 登录远程服务器的密码
     */
    private String password;

    /**
     * 登录远程服务器的公钥
     */
    private String publicKey;

}
