package com.at.repo.entity.gitlab;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 徐旻
 * @since 2022-07-22 17:38:35
 */
@Getter
@Setter
@TableName("tb_gitlab_diff")
public class GitlabDiffEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 对应的执行id
     */
    private Integer execId;

    /**
     * git diff对应的commit id
     */
    private String commitId;

    /**
     * 修改的内容的文件名
     */
    private String newPath;

    /**
     * 修改的内容的文件名(进行过格式化后）
     */
    private String path;

    /**
     * 修改内容的行数
     */
    private Integer line;

    /**
     * 修改内容的行数的长度
     */
    private Integer contentLength;


}
