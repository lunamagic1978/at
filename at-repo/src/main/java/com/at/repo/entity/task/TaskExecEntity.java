package com.at.repo.entity.task;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

/**
 * <p>
 * 
 * </p>
 *
 * @author 徐旻
 * @since 2022-05-25 10:06:31
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@TableName("tb_task_exec")
public class TaskExecEntity extends Model<TaskExecEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 对应的任务id
     */
    private Integer taskId;

    /**
     * 源分支
     */
    private String fromBranch;

    /**
     * 源分支对应的commit
     */
    private String fromBranchCommit;

    /**
     * 目标分支
     */
    private String toBranch;

    /**
     * 目标分支对应的commit
     */
    private String toBranchCommit;

    /**
     * jacocoAgent的IP地址
     */
    private String jacocoIp;

    /**
     * jacocoAgent的Port
     */
    private Integer jacocoPort;

    /**
     * jacoco分析需要的class路径
     */
    private String classPath;

    /**
     * jacoco分析需要jar包的路径
     */
    private String jarLocation;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 创建者
     */
    private String creater;

    /**
     * 任务状态 0：停止 1：执行中
     */
    private Integer status;

    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer isDelete;

    /**
     * 获取GitCompare内容时候的commitId
     */
    private String commitId;

    /**
     * 获取jar包的状态，本地jar包不需要
     * 0:  下载中
     * 1:  解压中
     * 2:  可用
     * 3:  异常
     */
    private Integer jarStatus;

    /**
     * 使用相关的JarPathConfig
     */
    private Integer jarPathConfig;

}
