package com.at.repo.entity.jacg;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 徐旻
 * @since 2022-07-19 16:51:30
 */
@Getter
@Setter
@TableName("tb_callee_chain")
public class CalleeChainEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 对应的任务id
     */
    private Integer taskId;

    /**
     * 对应的执行id
     */
    private Integer execId;

    /**
     * git diff对应的commit id
     */
    private String commitId;

    /**
     * 调用的层级
     */
    private String level;

    /**
     * 被调用函数的全名
     */
    private String calleeFullName;

    /**
     * 被调用函数的包名
     */
    private String calleePackageName;

    /**
     * 被调用函数的行号
     */
    private String line;

    /**
     * 调用改函数的id
     */
    private Integer parent;


}
