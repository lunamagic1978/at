package com.at.repo.entity.gitlab;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class GitlabTreeEntity implements Serializable {

    private String id;
    private String name;
    private String type;
    private String path;
    private String mode;
}
