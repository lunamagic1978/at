package com.at.repo.entity.task;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaskBranches {

    private String name;
    private TaskBranchesCommitEntity commit;
    private boolean merged;
    private boolean developers_can_push;
    private boolean developers_can_merge;
    private boolean can_push;
    private String web_url;
}