package com.at.repo.entity.task;
import lombok.*;

import java.util.ArrayList;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author 徐旻
 * @since 2022-05-25 10:06:31
 */
@Getter
@Setter
public class TaskBranchesCommitEntity {

    private String id;
    private String short_id;
    private Date created_at;
    private ArrayList parent_ids;
    private String title;
    private String message;
    private String author_name;
    private String author_email;
    private Date authored_date;
    private String committer_name;
    private String committer_email;
    private Date committed_date;
    private String web_url;

}
