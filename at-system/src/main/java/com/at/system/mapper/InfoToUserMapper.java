package com.at.system.mapper;

import com.at.system.entity.InfoToUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface InfoToUserMapper extends BaseMapper<InfoToUser> {
}
