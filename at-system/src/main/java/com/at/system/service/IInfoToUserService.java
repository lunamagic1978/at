package com.at.system.service;

import com.at.system.entity.InfoToUser;
import com.baomidou.mybatisplus.extension.service.IService;

public interface IInfoToUserService extends IService<InfoToUser> {
}
