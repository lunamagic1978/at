package com.at.system.service;

import com.alibaba.fastjson.JSONObject;
import com.at.system.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

public interface IUserService extends IService<User> {

    Map<String,Object> checkMobileAndPasswd(JSONObject requestJson)throws Exception;

    Map<String, Object> getLoginUserAndMenuInfo(User user);

}
