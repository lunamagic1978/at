package com.at.system.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.at.base.BusinessException;
import com.at.common.enums.CodeEnum;
import com.at.system.entity.InfoToUser;
import com.at.system.entity.User;
import com.at.system.mapper.UserMapper;
import com.at.system.service.IInfoToUserService;
import com.at.system.service.IUserService;
import com.at.util.ComUtil;
import com.at.util.JWTUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private IInfoToUserService infoToUserService;

    @Override
    public Map<String, Object> getLoginUserAndMenuInfo(User user) {
        Map<String, Object> result = new HashMap<>();
//        UserToRole userToRole = userToRoleService.selectByUserNo(user.getUserNo());
        user.setToken(JWTUtil.sign(user.getUserNo(), user.getPassword()));
        result.put("user",user);
//        List<Menu> buttonList = new ArrayList<Menu>();
//        //根据角色主键查询启用的菜单权限
//        List<Menu> menuList = menuService.findMenuByRoleCode(userToRole.getRoleCode());
//        List<Menu> retMenuList = menuService.treeMenuList(Constant.ROOT_MENU, menuList);
//        for (Menu buttonMenu : menuList) {
//            if(buttonMenu.getMenuType() == Constant.TYPE_BUTTON){
//                buttonList.add(buttonMenu);
//            }
//        }
//        result.put("menuList",retMenuList);
//        result.put("buttonList",buttonList);
        return result;
    }

    @Override
    public Map<String, Object> checkMobileAndPasswd(JSONObject requestJson) throws Exception{
        //由于 @ValidationParam注解已经验证过mobile和passWord参数，所以可以直接get使用没毛病。
        String identity = requestJson.getString("identity");

        InfoToUser infoToUser = infoToUserService.getOne((Wrapper<InfoToUser>) new QueryWrapper().eq("identity_info", identity));
//               InfoToUser infoToUser = infoToUserService.getOne(new QueryWrapper<>().eq("identity_info ", identity));
        if(ComUtil.isEmpty(infoToUser)){
            throw new BusinessException(CodeEnum.INVALID_USER.getMsg(),CodeEnum.INVALID_USER.getCode());
        }
        User user = this.getOne((Wrapper<User>) new QueryWrapper().allEq(new HashMap() {{
            put("user_no", infoToUser.getUserNo());
            put("status", "1");
        }}));
//        User user = this.selectOne(new EntityWrapper<User>().where("user_no = {0} and status = 1",infoToUser.getUserNo()));
        if (ComUtil.isEmpty(user) || !BCrypt.checkpw(requestJson.getString("password"), user.getPassword())) {
            throw new BusinessException(CodeEnum.INVALID_USERNAME_PASSWORD.getMsg(),CodeEnum.INVALID_USERNAME_PASSWORD.getCode());
        }
        //测试websocket用户登录给管理员发送消息的例子  前端代码参考父目录下WebSocketDemo.html
//        noticeService.insertByThemeNo("themeNo-cwr3fsxf233edasdfcf2s3","13888888888");
//        MyWebSocketService.sendMessageTo(JSONObject.toJSONString(user),"13888888888");
        return this.getLoginUserAndMenuInfo(user);
    }
}
