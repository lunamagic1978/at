package com.at.system.service.impl;

import com.at.system.entity.InfoToUser;
import com.at.system.mapper.InfoToUserMapper;
import com.at.system.service.IInfoToUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class InfoToUserServiceImpl extends ServiceImpl<InfoToUserMapper, InfoToUser> implements IInfoToUserService {
}
