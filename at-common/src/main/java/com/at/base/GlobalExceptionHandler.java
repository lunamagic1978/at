package com.at.base;

import com.at.common.enums.CodeEnum;
import com.at.config.ResponseHelper;
import com.at.config.ResponseModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.apache.shiro.authc.AuthenticationException;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value = BusinessException.class)
    public ResponseModel businessExceptionHandle (BusinessException e){
        System.out.println("businessExceptionHandle");
        return ResponseHelper.failedCodeMessage(e.getErrorCode(), e.getErrorMsg());
    }

    @ExceptionHandler(value = AuthenticationException.class)
    public ResponseModel authenticationExceptionHandle( AuthenticationException e){
        System.out.println("authenticationExceptionHandle");
        return ResponseHelper.failed2Message(e.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseModel execptExceptionHandle( Exception e){
        System.out.println("execptExceptionHandle");
        System.out.println(e);
        return ResponseHelper.failed2Message(e.getMessage());
    }

    @ExceptionHandler(value = ParamJsonException.class)
    public ResponseModel paramJsonExceptionHandle( Exception e){
        System.out.println("paramJsonExceptionHandle");
        return ResponseHelper.failedCodeMessage(CodeEnum.PARAM_ERROR.getCode(), e.getMessage());
    }

}
