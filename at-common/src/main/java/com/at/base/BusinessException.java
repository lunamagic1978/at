package com.at.base;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessException extends Exception{

    /**
     * Comment for &lt;code&gt;serialVersionUID&lt;/code&gt;
     */

    private static final long serialVersionUID = 3455708526465670030L;

    protected String errorCode;

    protected String errorMsg;

    public BusinessException(String msg){
        super(msg);
    }

    public BusinessException(String msg,String code){
        super(msg+":--:"+code);
        this.errorCode = code;
        this.errorMsg = msg;
    }
}
