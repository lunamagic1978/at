package com.at.base;

import lombok.Getter;
import lombok.Setter;

/**
 * 参数异常
 */
@Getter
@Setter
public class ParamJsonException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    private String errorMsg;

    private String errorCode;

    public ParamJsonException(String msg){
        super(msg);
    }

    public ParamJsonException(String msg, String code) {
        super(msg);
        this.errorMsg = msg;
        this.errorCode = code;
    }
}
