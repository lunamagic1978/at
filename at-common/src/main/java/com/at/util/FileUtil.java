package com.at.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;

public class FileUtil {

    private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

    public static boolean isDirectoryExists(String dirPath) {
        File file = new File(dirPath);
        if (file.exists()) {
            if (file.isDirectory()) {
                logger.info("directory exists: {}", dirPath);
                return true;
            }
            logger.error("file exists: {}", dirPath);
            return false;
        }

        try {
            Files.createDirectories(file.toPath());
            logger.info("create directory: {}", dirPath);
            return true;
        } catch (FileAlreadyExistsException e) {
            logger.warn("try to create directory but exists: {}", dirPath);
            return true;
        } catch (IOException e) {
            logger.error("error ", e);
            return true;
        }
    }

    /**
     * 根据远程服务器存放jarFIle的路径分析出jar包的名字和给的路径名字，返回存放jar的绝对路径
     * targetPath: /var/lib/jenkins/workspace/demo/target/demo-0.0.1-SNAPSHOT.jar
     * dirPath: /Users/xumin/jo/at/jarPath/task21/exec48
     * 返回值: /Users/xumin/jo/at/jarPath/task21/exec48/demo-0.0.1-SNAPSHOT.jar
     */
    public static String formatScpFileSavePath(String targetPath, String dirPath){
        String[] tmp = targetPath.split("/");
        System.out.println(tmp[tmp.length - 1]);
        return dirPath + "/" + tmp[tmp.length - 1];
    }
}
