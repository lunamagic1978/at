/*
 Navicat Premium Data Transfer

 Source Server         : 本机mysql
 Source Server Type    : MySQL
 Source Server Version : 50738
 Source Host           : localhost:3306
 Source Schema         : at

 Target Server Type    : MySQL
 Target Server Version : 50738
 File Encoding         : 65001

 Date: 27/07/2022 15:29:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_task_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_task_info`;
CREATE TABLE `tb_task_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_no` varchar(255) DEFAULT NULL,
  `task_name` varchar(255) DEFAULT NULL,
  `task_desc` varchar(255) DEFAULT NULL,
  `private_token` varchar(255) DEFAULT NULL,
  `web_host` varchar(255) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `jacoco_ip` varchar(255) DEFAULT NULL,
  `jacoco_port` int(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `class_path` varchar(255) DEFAULT NULL,
  `jar_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for tb_task_exec
-- ----------------------------
DROP TABLE IF EXISTS `tb_task_exec`;
CREATE TABLE `tb_task_exec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) DEFAULT NULL,
  `from_branch` varchar(255) DEFAULT NULL,
  `to_branch` varchar(255) DEFAULT NULL,
  `jacoco_ip` varchar(255) DEFAULT NULL,
  `jacoco_port` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `creater` varchar(255) DEFAULT NULL,
  `status` int(255) DEFAULT NULL,
  `is_delete` int(255) DEFAULT NULL,
  `class_path` varchar(255) DEFAULT NULL,
  `jar_location` varchar(255) DEFAULT NULL,
  `commit_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for tb_gitlab_diff
-- ----------------------------
DROP TABLE IF EXISTS `tb_gitlab_diff`;
CREATE TABLE `tb_gitlab_diff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exec_id` int(11) DEFAULT NULL,
  `commit_id` varchar(255) DEFAULT NULL,
  `new_path` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `line` int(11) DEFAULT NULL,
  `content_length` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=771 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for tb_callee_chain
-- ----------------------------
DROP TABLE IF EXISTS `tb_callee_chain`;
CREATE TABLE `tb_callee_chain` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) DEFAULT NULL,
  `exec_id` int(11) DEFAULT NULL,
  `level` varchar(255) DEFAULT NULL,
  `callee_full_name` varchar(255) DEFAULT NULL,
  `callee_package_name` varchar(255) DEFAULT NULL,
  `line` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `commit_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4;
